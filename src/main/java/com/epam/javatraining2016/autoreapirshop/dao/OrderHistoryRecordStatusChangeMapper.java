package com.epam.javatraining2016.autoreapirshop.dao;

public interface OrderHistoryRecordStatusChangeMapper {
  void insert(OrderHistoryRecordStatusChangeRow orderHistortyRow);

  OrderHistoryRecordStatusChangeRow selectShallow(int recordId);
}
