package com.epam.javatraining2016.autoreapirshop.dao;

public class ClientRow extends Row {
  private OrderListRow orderList = null;
  private String clientName = null;

  public ClientRow(Integer id) {
    super(id);
  }

  public OrderListRow getOrderList() {
    return orderList;
  }

  public void setOrderList(OrderListRow orderList) {
    this.orderList = orderList;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
}
