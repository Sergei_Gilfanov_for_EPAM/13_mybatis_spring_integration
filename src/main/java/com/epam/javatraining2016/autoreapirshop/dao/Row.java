package com.epam.javatraining2016.autoreapirshop.dao;

abstract class Row {
  private int id;

  protected Row(Integer id) {
    this.id = id.intValue();
  }

  public int getId() {
    return id;
  }
}
