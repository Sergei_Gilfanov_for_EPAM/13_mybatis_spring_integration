package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class OrderHistoryRecordStatusChangeDao {
  @Autowired
  private OrderHistoryRecordMapper parentMapper;
  @Autowired
  private OrderHistoryRecordStatusChangeMapper mapper;

  public OrderHistoryRecordStatusChangeRow create(OrderHistoryRow orderHistory,
      OrderStatusRow orderStatus, CommentRow comment) {
    OrderHistoryRecordStatusChangeRow retval = new OrderHistoryRecordStatusChangeRow(0);
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setOrderStatus(orderStatus);
    retval.setComment(comment);

    parentMapper.insert(retval);
    mapper.insert(retval);
    return retval;
  }

  @Cacheable("OrderHistoryRecordStatusChange")
  public OrderHistoryRecordStatusChangeRow getShallow(int recordId) {
    OrderHistoryRecordStatusChangeRow retval = mapper.selectShallow(recordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
