package com.epam.javatraining2016.autoreapirshop.dao;

public class OrderHistoryRecordStatusChangeRow extends OrderHistoryRecordRow {
  private OrderStatusRow orderStatus;
  private CommentRow comment;

  OrderHistoryRecordStatusChangeRow(Integer id) {
    super(id);
  }

  public OrderStatusRow getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatusRow orderStatus) {
    this.orderStatus = orderStatus;
  }

  public CommentRow getComment() {
    return comment;
  }

  public void setComment(CommentRow comment) {
    this.comment = comment;
  }

}
