package com.epam.javatraining2016.autoreapirshop.dao;

public interface ClientMapper {
  void insert(ClientRow client);

  ClientRow selectShallow(int clientId);
}
