package com.epam.javatraining2016.autoreapirshop.dao;

public interface OrderHistoryRecordCommentMapper {
  void insert(OrderHistoryRecordCommentRow orderHistortyRow);

  OrderHistoryRecordCommentRow selectShallow(int recordId);
}
